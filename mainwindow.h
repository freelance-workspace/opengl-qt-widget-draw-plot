#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

public slots:


private slots:

    void on_horizontalSlider_2_sliderMoved(int position);

    void on_checkBox_toggled(bool checked);

    void on_pushButton_clicked();

    void on_horizontalSlider_3_valueChanged(int value);

    void on_verticalSlider_valueChanged(int value);

    void on_pushButton_2_clicked();

    void on_checkBox_Dashed_toggled(bool checked);

    void on_checkBox_DrawPoints_toggled(bool checked);

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
