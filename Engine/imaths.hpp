#ifndef IMATHS_HPP
#define IMATHS_HPP

#include "IMath/IFunc.h"
#include "IMath/IVector2D.h"
#include "IMath/IVector3D.h"
#include "IMath/IVector4D.h"
#include "IMath/IMatrix2x2.h"
#include "IMath/IMatrix3x3.h"
#include "IMath/IMatrix4x4.h"
#include "IMath/IQuaternion.h"
#include "IMath/ITransform.h"
#include "IMath/IAffineTransform.h"
#include "IMath/ISpherical.h"
#include "IMath/IVector.h"
#include "IMath/IPlane.h"


#include <limits>


namespace IEngine
{

typedef float scalar;


typedef IEngine::IVector2D<scalar>        Vector2;
typedef IEngine::IVector3D<scalar>        Vector3;
typedef IEngine::IVector4D<scalar>        Vector4;
typedef IEngine::IMatrix2x2<scalar>       Matrix2;
typedef IEngine::IMatrix3x3<scalar>       Matrix3;
typedef IEngine::IMatrix4x4<scalar>       Matrix4;
typedef IEngine::IQuaternion<scalar>      Quaternion;
typedef IEngine::ITransform<scalar>       Transform;
typedef IEngine::IAffineTransform<scalar> AffineTransform;

typedef IEngine::IPlane<scalar>           Plane;
typedef IEngine::IPlaneCoplanar<scalar>   PlaneCoplanar;


typedef IEngine::IVector2D<int> Index2i;
typedef IEngine::IVector3D<int> Index3i;
typedef IEngine::IVector4D<int> Index4i;

typedef IEngine::IVector2D<int> Vector2i;
typedef IEngine::IVector3D<int> Vector3i;
typedef IEngine::IVector4D<int> Vector4i;

const scalar DECIMAL_SMALLEST = -std::numeric_limits<scalar>::max();
const scalar DECIMAL_LARGEST  =  std::numeric_limits<scalar>::max();

}




#endif // IMATHS_HPP
