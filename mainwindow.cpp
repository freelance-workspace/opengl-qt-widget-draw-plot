#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QLineEdit>
#include <QLabel>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Graphics Function Widget");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    ui->widget_plot->keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    ui->widget_plot->keyReleaseEvent(event);
}


void MainWindow::on_horizontalSlider_2_sliderMoved(int position)
{
    qDebug() << position;
    ui->widget_plot->scene()->graphPlots()->setWidthLine(position);
}


void MainWindow::on_checkBox_toggled(bool checked)
{
    ui->widget_plot->scene()->graphPlots()->setIsFill(checked);
}


void MainWindow::on_pushButton_clicked()
{
    qDebug() << ui->pushButton->getColor();

    Vector4 color(
    ui->pushButton->getColor().redF(),
    ui->pushButton->getColor().greenF(),
    ui->pushButton->getColor().blueF(),
    1.0);
    ui->widget_plot->graphPlots()->setColor(color);

}


void MainWindow::on_horizontalSlider_3_valueChanged(int value)
{
    float v = value / 10.f;
    qDebug() << v;
    ui->widget_plot->graphPlots()->setScale_X( v );
}


void MainWindow::on_verticalSlider_valueChanged(int value)
{
    qDebug() << value/50.f;
    ui->widget_plot->graphPlots()->setScale_Y( value/10.f );
}


void MainWindow::on_pushButton_2_clicked()
{
    ui->widget_plot->graphPlots()->setType(Orintation_Plot::HORIZONTAL);
    ui->widget_plot->scene()->setIsEnableScrollingX(true);
    ui->widget_plot->scene()->setIsEnableScrollingY(true);
    ui->widget_plot->scene()->setIsEnableZoom(true);
    ui->widget_plot->scene()->setZoom(2);
    //ui->widget_plot->scene()->camera().SetOrthographic(true);

    float dt = 1.f/50.f;
    ui->widget_plot->graphPlots()->setStepDifferent(dt);
    ui->widget_plot->graphPlots()->points().clear();

    // initilization graphyics random points
    for (int i = -1000000/2,j=0; i < 1000000/2; ++i,++j)
    {
       float y = cos(i * 1.f/10.f) * ((i%1==0)? float(1+rand()%100)/200.f : 1.f);
       float x = i * 1.f/50.f;///100.f;
       ui->widget_plot->graphPlots()->AppendPoint(Vector2(x,y));
       //mGraphPlots->AppendPoint(Vector2(y,x) + Vector2(0.001,0));
    }
}


void MainWindow::on_checkBox_Dashed_toggled(bool checked)
{
    ui->widget_plot->graphPlots()->setIsDashed(checked);
}


void MainWindow::on_checkBox_DrawPoints_toggled(bool checked)
{
   ui->widget_plot->graphPlots()->setIsDrawPoint(checked);
}

