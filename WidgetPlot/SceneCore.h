#ifndef SCENECORE_H
#define SCENECORE_H


#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLFunctions_4_3_Compatibility>

#include "Engine/IQCamera.h"
#include "Shader/Shader.h"
#include "IPlotFunctionGraph2D.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>
#include <string>

class UnitKey
{
  protected:

    bool mKeys[256];
    int  mMouseButton;

  public:

    void specialKeyboardDown(int key)
    {
         mKeys[key] = true;
    }

    void specialKeyboardUp(int key)
    {
         mKeys[key] = false;
    }

    void NullAllKey()
    {
        for (int i = 0; i < 256; ++i) mKeys[i] = false;
    }

    bool keyDown( int key )
    {
        return mKeys[key];
    }

    int mouseButton()
    {
        return mMouseButton;
    }

    void setMouseButton(int newMouseButton)
    {
        mMouseButton = newMouseButton;
    }
};


/**
 * \authors werasaimon
 * \version 0.01
 * @brief The SceneCore class
 *  drawing and displaying the plot scene
 */
class SceneCore : protected QOpenGLFunctions , public UnitKey
{

private :

    /// \brief The Mouse struct - values ​​for mouse
    struct Mouse
    {
        float mouseX;
        float mouseY;

        float Old_mouseX;
        float Old_mouseY;

    }  data_mouse;

    float mWidth; //< Widget Width
    float mHeight; // < Widget Height

    float mScrollingSpeed; //< Scrolling speed a mouse
    float mWhellScale; //< Scale with mouse wheel
    float mWhellCofficient; //< Cofficient with mouse wheel for scale

    bool  m_isEnableScrollingX; //< Enable scale axis-X
    bool  m_isEnableScrollingY; //< Enable scale axis-Y
    bool  m_isEnableZoom; //< Zoom with mouse wheel
    bool  m_isEnableWhellScale; //< Enable zoom with mouse wheel
    //-----------------------------//

    QOpenGLFunctions_4_3_Compatibility* ogl; //< OpenGL Functions
    IPlotFunctionGraph *mGraphPlots; //< Object graphics
    GLShaderProgram     mProgramShader; //< Program Shader


    GLShaderProgram     mProgramShaderPoint; //< Program Shader

    //-----------------------------//

    float mCameraZDistance; //< Distance camera Z-axis a Zoom
    float mCameraShiftLeft; //< camera shift X-axis
    float mcameraShiftUp; //<  camera shift Y-axis
    //-----------------------------//

    IQCamera mCamera; //< Scene view camera

    Vector3 mEye; //< camera location point
    Vector3 mTarget; //< target point where the camera is looking
    Vector3 mUp; //< camera lens orientation

    //-----------------------------//

    float mMaxWhellScale; //< Maximum scale

public:


    /// \brief SceneCore - Scene Constructor
    /// \param w - Widget Width
    /// \param h - Widget Height
    SceneCore(float w,float h);


    /// Initializing camera values
    void initCamera();

    /// initializing Program Shader
    void initShader();

    ///initializing scene values
    void initialization();

    ///
    /// \brief render - Draw Scene
    /// \param FrameTime - 1.0/FPS
    void render(float FrameTime);

    /// Update timer scene
    void update();


    /// \brief resize -  Resize scene
    /// \param width - Widget Width
    /// \param height - Widget Height
    void resize( float width , float height );

    ///
    /// \brief mouseMove - Event mouse move
    /// \param x - mouseX
    /// \param y - mouseY
    /// \param button - mouseButton
    void mouseMove( float x , float y  , int button);

    /// \brief mouseMove - Event mouse pressed key
    /// \param x - mouseX
    /// \param y - mouseY
    /// \param button - mouseButton
    void mousePress( float x , float y , int button );

    /// \brief mouseMove - Event mouse release
    /// \param x - mouseX
    /// \param y - mouseY
    /// \param button - mouseButton
    void mouseReleasePress( float x , float y , int button );

    ///
    /// \brief mouseWheel - Event mouse-whell
    /// \param delta - mouse wheel scroll speed
    void mouseWheel( float delta );

    ///
    /// \brief keyboard - event keybord
    /// \param key - signal key
    void keyboard(int key );

    /// Destroy Scene
    void destroy();


    /// \brief graphPlots - render graphics object into scenes
    /// \return  render graphics
    IPlotFunctionGraph *graphPlots() const;

    /// \brief setIsEnableScrollingX - Enable scale axis-X
    /// \param newIsEnableScrollingX - on/off
    void setIsEnableScrollingX(bool newIsEnableScrollingX);

    /// \brief setIsEnableScrollingY - Enable scale axis-Y
    /// \param newIsEnableScrollingY - on/off
    void setIsEnableScrollingY(bool newIsEnableScrollingY);


    /// \brief setIsEnableZoom - Enable zoom with mouse wheel
    /// \param newIsEnableZoom - on/off
    void setIsEnableZoom(bool newIsEnableZoom);

    /// \brief setZoom - Zoom a plot
    /// \param distance - Distance camera Z-Axis or Zoom
    void setZoom(float distance);


    /// \brief setScrollingSpeed - Scrolling speed a mouse
    /// \param newScrollingSpeed - Scrolling speed value
    void setScrollingSpeed(float newScrollingSpeed);


    /// \brief setWhellCofficient - Cofficient speed wheel mouse
    /// \param newWhellCofficient - speed wheel mouse value
    void setWhellCofficient(float newWhellCofficient);


    /// \brief setIsEnableWhellScale - Enable zoom with mouse wheel
    /// \param newIsEnableWhellScale - on/off
    void setIsEnableWhellScale(bool newIsEnableWhellScale);


    /// \brief setMaxWhellScale - Maximum scale
    /// \param newMaxWhellScale - Maximum scale value
    void setMaxWhellScale(float newMaxWhellScale);

    //// Camera 3D-Space
    IQCamera &camera();
};

#endif // SCENECORE_H
