#include "SceneCore.h"
#include <QFile>

#include <QDebug>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLContext>


IQCamera &SceneCore::camera()
{
    return mCamera;
}

SceneCore::SceneCore(float w,float h) :
 mWidth(w),
 mHeight(h)
{
    //Initialize OpenGL functions
    initializeOpenGLFunctions();

    ///Object a OpenGL Functions
    ogl = new QOpenGLFunctions_4_3_Compatibility;
    ogl->initializeOpenGLFunctions();

    // Initilization graphic func
    mGraphPlots = new IPlotFunctionGraph();
}

void SceneCore::initCamera()
{
    mWidth  = 600;
    mHeight = 400;

    float aspect = mWidth / mHeight;
    float zNear  = 0.02;
    float zFar   = 2500;
    float fov    = 30.0;

    mCamera.SetAspect(aspect);
    mCamera.SetNear(zNear);
    mCamera.SetFar(zFar);
    mCamera.SetAngle(fov);

    mEye = (Vector3(0,0,mCameraZDistance = 20));
    mTarget = (Vector3(0,0,0));
    mUp = (Vector3(0,1,0));

}

void SceneCore::initShader()
{

    // Compile vertex shader
    if (!mProgramShader.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vplot.glsl"))
        qDebug() << "Error";

    // Compile fragment shader
    if (!mProgramShader.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fplot.glsl"))
        qDebug() << "Error";

    // Link shader pipeline
    if (!mProgramShader.link())
        qDebug() << "Error";

    //=================================================================================================//

    // Compile vertex shader
    if (!mProgramShaderPoint.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vpoint.glsl"))
        qDebug() << "Error";

    // Compile fragment shader
    if (!mProgramShaderPoint.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fpoint.glsl"))
        qDebug() << "Error";

    // Link shader pipeline
    if (!mProgramShaderPoint.link())
        qDebug() << "Error";

}



void SceneCore::initialization()
{
    mWidth  = 600;
    mHeight = 400;
    mWhellScale = 1.f;
    mWhellCofficient = 1.f;
    mCameraZDistance = 5;
    mScrollingSpeed = 1.0/100.f;
    m_isEnableZoom = m_isEnableScrollingX = m_isEnableScrollingY = true;
    mMaxWhellScale = 50.f;

    //===========================//

    NullAllKey();
    initShader();
    initCamera();

    //===========================//

    /// Default value line width = 5
    mGraphPlots->setWidthLine(5);

}

void SceneCore::render(float FrameTime)
{

    mEye  = mTarget = Vector3::ZERO;
    mEye += Vector3::Y * mcameraShiftUp;
    mEye += Vector3::X * mCameraShiftLeft;
    mTarget = mEye = mEye + Vector3::Z * mCameraZDistance;


    mCamera.LookAt(mEye, mTarget , mUp);

    //========== Render OpenGL Scene Plot ================//
    if(mGraphPlots)
    {
       if(mGraphPlots->isDrawPoint())
       {
          mGraphPlots->RenderGL_Points(mCamera,mWidth , mHeight,&mProgramShaderPoint,ogl);
       }

        mGraphPlots->RenderShader(mCamera,mWidth , mHeight,&mProgramShader,ogl);
    }
    //====================================================//
}

void SceneCore::update()
{
    if(mGraphPlots)
    {
        mGraphPlots->Update();
    }
}

void SceneCore::resize(float width, float height)
{
    mWidth  = width;
    mHeight = height;

    float aspect = mWidth / mHeight;
    float zNear  = 1.0;
    float zFar   = 250;
    float fov    = 30.0;

    mCamera.SetAspect(aspect);
    mCamera.SetNear(zNear);
    mCamera.SetFar(zFar);
    mCamera.SetAngle(fov);
}

void SceneCore::mouseMove(float x, float y, int button)
{
    data_mouse.mouseX = x;
    data_mouse.mouseY = y;

    if( mMouseButton == Qt::MouseButton::RightButton )
    {
        float speedX = (data_mouse.mouseX - data_mouse.Old_mouseX);
        float speedY = (data_mouse.mouseY - data_mouse.Old_mouseY);

        float coff = mScrollingSpeed;

       if(m_isEnableScrollingX) mCameraShiftLeft += speedX*coff;
       if(m_isEnableScrollingY) mcameraShiftUp   += speedY*coff;

        data_mouse.Old_mouseX = data_mouse.mouseX = x;
        data_mouse.Old_mouseY = data_mouse.mouseY = y;
    }
}

void SceneCore::mousePress(float x, float y, int button)
{
    UnitKey::setMouseButton( button );
    data_mouse.Old_mouseX = data_mouse.mouseX = x;
    data_mouse.Old_mouseY = data_mouse.mouseY = y;
}

void SceneCore::mouseReleasePress(float x, float y, int button)
{
    UnitKey::setMouseButton( button );
    data_mouse.Old_mouseX = data_mouse.mouseX = x;
    data_mouse.Old_mouseY = data_mouse.mouseY = y;
}

void SceneCore::mouseWheel(float delta)
{
    if(m_isEnableZoom)
    {
        mCameraZDistance += (delta * 0.02f);

        if(mCamera.isOrthographic())
        {
            float scale = (mCameraZDistance >= 0) ? mCameraZDistance : 1.0 / mCameraZDistance;
            mGraphPlots->setModelMatrix(Matrix4::CreateScale(scale));
        }
        else
        {
           mCameraZDistance = IEngine::IClamp(IEngine::IAbs(mCameraZDistance),1.f,1024.f);
        }
    }
    else if(m_isEnableWhellScale)
    {
        mWhellScale += (delta * 0.001f);
        float scale = IClamp( mWhellScale , 0.02f , mMaxWhellScale) * mWhellCofficient;

        qDebug() << scale;

        if(mGraphPlots->type() == Orintation_Plot::HORIZONTAL)
        {
            graphPlots()->setScale_X(scale);
        }
        else if(mGraphPlots->type() == Orintation_Plot::VERTICAL)
        {
            graphPlots()->setScale_Y(scale);
        }
    }
}

void SceneCore::keyboard(int key)
{

}

void SceneCore::destroy()
{
    delete ogl;

    if(mGraphPlots)
    {
        delete mGraphPlots;
        mGraphPlots = nullptr;
    }
}

//=============================================//

IPlotFunctionGraph *SceneCore::graphPlots() const
{
    return mGraphPlots;
}

void SceneCore::setIsEnableScrollingX(bool newIsEnableScrollingX)
{
    m_isEnableScrollingX = newIsEnableScrollingX;
}

void SceneCore::setIsEnableScrollingY(bool newIsEnableScrollingY)
{
    m_isEnableScrollingY = newIsEnableScrollingY;
}

void SceneCore::setIsEnableZoom(bool newIsEnableZoom)
{
    m_isEnableZoom = newIsEnableZoom;
}

void SceneCore::setZoom(float distance)
{
    mCameraZDistance = distance;
}

void SceneCore::setScrollingSpeed(float newScrollingSpeed)
{
    mScrollingSpeed = newScrollingSpeed;
}

void SceneCore::setWhellCofficient(float newWhellCofficient)
{
    mWhellCofficient = newWhellCofficient;
}

void SceneCore::setIsEnableWhellScale(bool newIsEnableWhellScale)
{
    m_isEnableWhellScale = newIsEnableWhellScale;
}

void SceneCore::setMaxWhellScale(float newMaxWhellScale)
{
    mMaxWhellScale = newMaxWhellScale;
}


